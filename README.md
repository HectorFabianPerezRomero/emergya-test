# Emergya test #

This is the repository for the test list product

### Dependencies ###

* [Feature module](https://www.drupal.org/project/features)
* [Focal point module](https://www.drupal.org/project/focal_point)

### Installation ###

You can install this module after install dependencies like any other module in Drupal

### How it works? ###

After installation, this module will create next items:

1. Product content type
2. Image style for cards
3. View of list products and will set this view to frontpage
4. Styling product list
5. Before the node detail page will ask you to accept the disclaimer for view the content