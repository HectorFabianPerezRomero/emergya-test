(function ($, Drupal) {
  const acceptNodeViewMsg = 'You must to accept terms and conditions to view this product';
  const currentNodeProtected = drupalSettings.current_node_protected;

  /**
   * Function to show and return response from confirm function
   * @param {string} message Message to show in the alert
   * @returns void
   */
  const showAlert = (message) => {
    return confirm(message);
  };

  /**
   * Function to init the confirm action to view the node
   * @param {Object} nodes Nodes configuration
   */
  const initAskPopup = (nodes) => {
    if(nodes === null){
      nodes = {
        accepted_nodes: []
      };
    }
    const acceptedNodeView = showAlert(acceptNodeViewMsg);
    if(acceptedNodeView){
      nodes.accepted_nodes.push(drupalSettings.current_node_id);
      Cookies.set('accepted_products', JSON.stringify(nodes));
      removeOverlay();
    }else{
      document.location.href='/';
    }
  };

  /**
   * Show overlay over the content
   */
  const showOverlay = () => {
    $('body').append('<div class="overlay"><div>');
    $('body').addClass('block-scroll');
  };

  showOverlay();

  /**
   * Remove overlay over the content
   */
  const removeOverlay = () => {
    $('body').removeClass('block-scroll');
    $('.overlay').remove();
  };

  /**
   * Drupal event to check cookies
   */
  Drupal.behaviors.initCheckCookie = {
    attach: function (context, settings) {
      $('.node--type-product', context).once('cookie-initialized').each(function(index, item){

        if(currentNodeProtected){
          const cookieNodes = Cookies.get('accepted_products');
          let nodes = null;
          if(typeof cookieNodes !== 'undefined' && cookieNodes !== null){
            nodes = JSON.parse(cookieNodes);
            if(!nodes.accepted_nodes.includes(drupalSettings.current_node_id)){
              initAskPopup(nodes);
            }else{
              removeOverlay();
            }
          }else{
            initAskPopup(nodes);
          }
        }else{
          removeOverlay();
        }
      });
    }
  };
})(jQuery, Drupal);