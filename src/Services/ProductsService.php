<?php

namespace Drupal\test_september_2021;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Logger\LoggerChannel;

/**
 * Class ProductsService.
 */
class ProductsService implements ProductsServiceInterface {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Datetime\DateFormatterInterface definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Drupal\Core\Logger\LoggerChannel definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  public $logger;

  /**
   * Constructs a new ProductsService object.
   */
  public function __construct(MessengerInterface $messenger, DateFormatterInterface $date_formatter,
    LoggerChannel $logger) {
    $this->messenger = $messenger;
    $this->dateFormatter = $date_formatter;
    $this->logger = $logger;
  }

}
